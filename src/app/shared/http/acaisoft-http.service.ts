import { Injectable } from "@angular/core";
import { Headers, Http, Response } from "@angular/http";
import { LoginRequest } from "../../models/login-request";
import { Observable } from "rxjs/Observable";
import { Book } from "../../models/book";
import { Subscriber } from "rxjs/Subscriber";
import { LoggedUserService } from "../logged-user/logged-user.service";

@Injectable()
export class AcaisoftHttpService {
  constructor(private http: Http,
              private loggedUserService: LoggedUserService) {
  }

  /*
   If the Response has an error it tries to extract its message.
   */
  private static handleError(response: Response): Observable<string> {
    return Observable.create((sub: Subscriber<string>) => {
      let errorMessage;
      try {
        errorMessage = response.json().message;
      } catch (e) {
        errorMessage = response.text();
      }
      sub.error(errorMessage);
    });
  }

  private generateJwtHeader(): Headers {
    if (!this.loggedUserService.getLoggedUser()) {
      return new Headers();
    }
    return new Headers({JWT_TOKEN: this.loggedUserService.getLoggedUser().jwt});
  }

  public getBookslist(): Observable<Book[]> {
    return this.http.get("/api/bookslist", {headers: this.generateJwtHeader()})
      .map(response => response.json())
      .catch(AcaisoftHttpService.handleError);
  }

  public borrowBook(bookId: string): Observable<Response> {
    return this.http.post(`/api/bookslist/${bookId}/borrow`, {}, {headers: this.generateJwtHeader()})
      .catch(AcaisoftHttpService.handleError);
  }

  public login(credentials: LoginRequest): Observable<Response> {
    return this.http.post("/api/user/login", credentials)
      .catch(AcaisoftHttpService.handleError);
  }
}
