import { Injectable } from '@angular/core';
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { LoggedUser } from "../../models/logged-user";
import { Observable } from "rxjs/Observable";

@Injectable()
export class LoggedUserService {
  private loggedUserObs: BehaviorSubject<LoggedUser | null> = new BehaviorSubject(null);

  private static readonly STORAGE_KEY = "acs_loggedUser";

  constructor() {
    this.retrieveUserFromStorage();
  }

  public setLoggedUser(user: LoggedUser): void {
    this.loggedUserObs.next(user || null);
    this.saveUserToStorage();
  }

  public getLoggedUser(): LoggedUser | null {
    return this.loggedUserObs.getValue();
  }

  public isLoggedUser(): boolean {
    return !!this.loggedUserObs.getValue();
  }

  public subscribeToLoggedUser(): Observable<LoggedUser | null> {
    return this.loggedUserObs;
  }


  private saveUserToStorage(): void {
    sessionStorage.setItem(LoggedUserService.STORAGE_KEY, JSON.stringify(this.getLoggedUser()));
  }

  private retrieveUserFromStorage(): void {
    const rawStorageUser = sessionStorage.getItem(LoggedUserService.STORAGE_KEY);

    try {
      let storageUser: LoggedUser = JSON.parse(rawStorageUser);
      this.setLoggedUser(storageUser);

    } catch (e) {
      console.error(`Cannot parse logged user from storage: ${rawStorageUser}`);
    }
  }
}
