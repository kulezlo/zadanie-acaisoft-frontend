import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";

import { AppComponent } from "./app.component";
import { LoginComponent } from "./pages/login/login.component";
import { RouterModule } from "@angular/router";
import { AppRoutes } from "./app.routes";
import { BooksListComponent } from "./pages/books-list/books-list.component";
import { LoggedUserService } from "./shared/logged-user/logged-user.service";
import { AuthGuard } from "./shared/guards/auth/auth.guard";
import { UnauthGuard } from "./shared/guards/unauth/unauth.guard";
import { AcaisoftHttpService } from "./shared/http/acaisoft-http.service";

import "rxjs";
import { LayoutComponent } from "./pages/layout/layout.component";
import { NavbarComponent } from "./pages/layout/navbar/navbar.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { SimpleNotificationsModule } from "angular2-notifications";
import { NotificationsService } from "angular2-notifications/dist";
import { RegisterComponent } from './pages/register/register.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    BooksListComponent,
    LayoutComponent,
    NavbarComponent,
    RegisterComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    RouterModule.forRoot(AppRoutes),
    BrowserAnimationsModule,
    SimpleNotificationsModule
  ],
  providers: [
    LoggedUserService,
    AcaisoftHttpService,
    AuthGuard,
    UnauthGuard,
    NotificationsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
