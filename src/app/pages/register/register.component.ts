import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from "@angular/forms";
import { Http } from "@angular/http"
import { Router } from "@angular/router";
import { NotificationsService } from "angular2-notifications/dist";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  formGroup: FormGroup;

  private email: FormControl;
  private password: FormControl;
  private confirmPassword: FormControl;
  private captcha: FormControl;
  private formId: string;
  private captchaValue: string;

  constructor(private http: Http,
              private router: Router,
              private notificationsService: NotificationsService)
  {
    this.email = new FormControl("", [Validators.required, Validators.email]);
    this.password = new FormControl("", Validators.required);
    this.confirmPassword = new FormControl("", Validators.required);
    this.captcha = new FormControl("", [Validators.required, (c) => this.captchaValidator(c as FormControl)]);
    this.captchaValue = "Captcha";
  }

  ngOnInit() {
    this.formGroup = new FormGroup({
      email: this.email,
      password: this.password,
      confirmPassword: this.confirmPassword,
      captcha: this.captcha
    }, this.passwordMatchValidator);

    this.http.get(
      '/api/user/register/captcha/generate',
    )
      .map(res => res.json())
      .subscribe(res => {
        this.formId = res['formId'];
        this.captchaValue = res['textValue'];
      }, error => {
        console.error("Captcha Error", error);
      });
  }

  signUp(): void {
    if (this.formGroup.invalid) {
      this.markControls(this.formGroup, control => control.markAsDirty());
      console.warn("Cannot sign up, formGroup is invalid!");
      return;
    }

    this.http.get(
      '/api/user/exists/' + encodeURI(this.email.value),
    )
      .map(res => res.json())
      .subscribe(res => {
        if (res['userExists'] === true)
          this.notificationsService.warn("Warning!", "User already exists");
        else
          this.sendPost();
      }, error => {
        console.error("Error", error);
        this.notificationsService.error("Error", error);
      });
  }

  private markControls(form: FormGroup, callback: (control: AbstractControl) => void): void {
    Object.keys(form.controls)
      .forEach(key => {
        callback(form.get(key));
      })
  }

  private passwordMatchValidator(g: FormGroup): object {
    let confirm = g.get('confirmPassword');
    if(g.get('password').value === confirm.value) {
        if(confirm.value.length)
          confirm.setErrors(null);
        return null;
    } else {
        confirm.setErrors({'mismatch': true});
        return {'mismatch': true};
    }
  }

  private captchaValidator(c: FormControl): object {
    if(c.value === this.captchaValue)
      return null;
    else
      return {'captchaInvalid': true};
  }

  private sendPost(): void {
    this.http.post(
      '/api/user/register/captcha',
      JSON.stringify({
        "email": this.email.value,
        "password": this.password.value,
        "captcha": {
          "formId": this.formId,
          "textValue": this.captchaValue
        }
      }))
      .subscribe(() => {
        console.log("Sign up success!");
        this.notificationsService.success("Sign up success!", "You can now log in");
        this.router.navigate(["/login"]);
      }, error => {
          console.error("Sign up request failed, reason: ", error);
          this.notificationsService.error("Error", error);
      });
  }

}
