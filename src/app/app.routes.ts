import { Routes } from "@angular/router";
import { LoginComponent } from "./pages/login/login.component";
import { BooksListComponent } from "./pages/books-list/books-list.component";
import { AuthGuard } from "./shared/guards/auth/auth.guard";
import { UnauthGuard } from "./shared/guards/unauth/unauth.guard";
import { LayoutComponent } from "./pages/layout/layout.component";
import { RegisterComponent } from "./pages/register/register.component";

export const AppRoutes: Routes = [
  {
    path: "",
    pathMatch: "full",
    redirectTo: "bookslist"
  },
  {
    canActivate: [UnauthGuard],
    path: "login",
    component: LoginComponent
  },
  {
    canActivate: [UnauthGuard],
    path: "register",
    component: RegisterComponent
  },
  {
    canActivate: [AuthGuard],
    path: "",
    component: LayoutComponent,
    children: [
      {
        path: "bookslist",
        component: BooksListComponent,
      }
    ],
  }
];
